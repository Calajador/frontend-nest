import { Injectable } from '@angular/core';
import { WebRequestService } from '../../services/web-request.service';
import { map, tap, switchMap } from 'rxjs/operators';
import { Observable } from 'rxjs';

@Injectable({
  providedIn: 'root',
})
export class AuthService {
  public token;

  constructor(private _web: WebRequestService) {}

  loginUser(user) {
    return this._web.post('users/login', user).pipe(
      map((token) => {
        console.log('token' + token);
        localStorage.setItem('token', token.access_token);
        return token;
      })
    );
  }

  isLogged() {
    return !!localStorage.getItem('token');
  }

  getToken(): Observable<any> {
    let token = localStorage.getItem('token');
    if (token) {
      this.token = token;
    } else {
      this.token = null;
    }

    return this.token;
  }

  logOutUser() {
    localStorage.removeItem('token');
  }
}
