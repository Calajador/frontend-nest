import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { AlertService } from 'src/app/shared/services/alert.service';
import { AuthService } from '../services/auth.service';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.scss'],
})
export class LoginComponent implements OnInit {
  secret: any;
  hide: boolean;
  loginUser = {
    email: null,
    password: null,
  };
  constructor(
    private _auth: AuthService,
    private router: Router,
    private _alerts: AlertService
  ) {}

  ngOnInit(): void {
    this.hide = true;
  }

  login() {
    this._auth.loginUser(this.loginUser).subscribe((res) => {
      this.router.navigate(['/']);
    });
  }
}
