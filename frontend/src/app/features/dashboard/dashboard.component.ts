import { Component, OnInit } from '@angular/core';
import {
  ColorButtonEnum,
  TypeButtonEnum,
} from 'src/app/shared/components/button/button.component';

@Component({
  selector: 'app-dashboard',
  templateUrl: './dashboard.component.html',
  styleUrls: ['./dashboard.component.scss'],
})
export class DashboardComponent implements OnInit {
  public readonly ButtonTypes = TypeButtonEnum;
  public readonly ButtonColors = ColorButtonEnum;

  public exampleColumns = {
    key1: 'Columna 1',
    key2: 'Columna 2',
    key3: 'Columna 3',
    key4: 'Columna 4',
    key5: 'Columna 5',
    key6: 'Columna 6',
    Editar: 'grid.edit',
  };

  public exampleTableData$: any[] = [];

  constructor() {}

  ngOnInit(): void {}
}
