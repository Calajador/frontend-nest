import { NavItem } from '../models/navItem.model';

export const menuItems: NavItem[] = [
  {
    displayName: 'sidebar.dashboard.menu_title',
    iconName: 'home',
    route: '',
  },
];
