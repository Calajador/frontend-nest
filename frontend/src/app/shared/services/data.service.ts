import { Injectable } from '@angular/core';
import { Observable, of } from 'rxjs';
import { menuItems } from '../data/sidenav.data';
import { NavItem } from '../models/navItem.model';

@Injectable({
  providedIn: 'root',
})
export class DataService {
  constructor() {}

  getmenuData(): Observable<NavItem[]> {
    return of(menuItems);
  }
}
