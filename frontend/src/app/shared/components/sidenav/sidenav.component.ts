import { Component, OnInit } from '@angular/core';
import { NavItem } from '../../models/navItem.model';
import { DataService } from '../../services/data.service';

@Component({
  selector: 'app-sidenav',
  templateUrl: './sidenav.component.html',
  styleUrls: ['./sidenav.component.scss'],
})
export class SidenavComponent implements OnInit {
  public navItems: NavItem[] | undefined;
  panelOpenState = false;

  constructor(private _data: DataService) {}

  ngOnInit(): void {
    this.getSidenavitems();
  }

  getSidenavitems() {
    this._data.getmenuData().subscribe((res: NavItem[]) => {
      this.navItems = res;
    });
  }
}
